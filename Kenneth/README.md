# Kenneth Medland

## Looks

- Height: 1.86m
- Eyes: Brown
- Hair: Brown
- Weight: 70kg
- Demeanor: Strict
- Ambition: Finding rank and status inside the ministorum so he can open up his
    own orphanage, as big as possible, possibly even as big as an orphanage
    planet

## Attributes

- BS: 22
- WS: 24
- S: 24
- A: 24
- T: 30
- Int: 35
- Will: 33
- Fel: 31 +5(Class) +3(Regiment) +3(Regiment) +5(1) +5(2) = 52
- Per: 26 -3(Regiment) = 23
- Wounds: 9 +3 +2 = 15
- Fate: 1 (1-7: 1, 8-9: 2, 10: 3)

## Aptitudes

- Willpower(Regiment)
- Fellowship
- Intelligence
- Knowledge
- Toughness(double Willpower from Priest & Regiment)
- Social
- WS
- Perception

- Wounds: 9 + 3 +2 = 15
- Fate: 1 (1-7: 1, 8-9: 2, 10: 3)

## Skills

- Athletics
- Low Gothic
- Operate (Surface) trained (+10)
- Charm +10
- Command +10
- Intimidate +10
- Common Lore (War, Imperial Guard, Ecclesiarchy)
- Forbidden Lore (Heresy, Xenos, Demons, Pskyers)
- Scholastic Lore (Imperial Creed, Beasts)
- Parry

## Talents

- Rapid Reload(Regiment)
- Hatred (Traitors)
- Unshakable Faith
- Weapon Training (Chain, Las, Flame, Low-Tech)
- Rightous Oration
- Walking Archive
- Know the Heretic; Kill the Heretic
- Air of Authority
- Radiant Presence

## Equipment

- Good quality hand flamer
- Book of Scripture
- Book of Lore
- Rosarius

- Flak Armor(+4 all)
- M36 Lasgun, 4 Chargepacks(Line infantry)
- Laspistol, 2 Chargepacks
- 2 Fragmentation Grenades &
- 2 Krak Grenades
- 1 Knife
- 1 Uniform
- 1 poor-weather gear
- Basic tool set
- Mess kit
- water canteen
- Blanket
- sleeping bag
- Rechargeable lamp pack
- Grooming kit
- ID tags
- Training handbook
- 2 weeks’ ration packs
- Rucksack
- Micro-bead
- Preysense Goggles
- Chrono

## Damage Log

- 3 Damage - rusty ladders
- Rosarius Overload - Magos blast
- 3 Damage - stray bullet while getting shot at
- 3 Damage - right arm while in cover

## Ammo Log

- 1 Shot from Las Pistol against a Servo Skull(miss) p120
- Burst of Flamer p130
- Burst of Flame against Cyber Ratling
- Burst of Flame to draw attention to let flankers sneak by p129

## FP Log

- -1 
- +1 FP Checkpoint after Magos battle
- -1
- +1 Turning point helping astartes while fleeing p131
- -1 FP Shake off fear from Nurgle monsters page 132
- +/-1 Save Sergeant Seth from getting overwhelmed by nurgle daemons, Page 132
- 1 BURNT to get out of the train station safely new total: 0/0

## Insanity Log

- +1 Page 128, ratling "hell"
- +3 Page 129, ratling trampled by marine stampede
- +2 Page 132, Vomiting crawling things

## Mission Rewards

### AdMech hive - steal the data

Experience: 1000xp

- 50% Forb. Lore Warp raise
- 1 permanent FP & -2 Corruption (for saving so many believers)
- 50% off next Command or Charm advance

## Links

- [Creation](Creation.md)
- [Weapons](Weapons.md)
- [Experience](Experience.md)
- [Backstory](Backstory.md)
- [Sermons](Sermons.md)
