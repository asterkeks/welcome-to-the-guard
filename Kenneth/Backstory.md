# Kenneth Medland - Backstory

Kenneth was born as the son of manufactorum workers. Both his father and mother
died in unrelated accidents and left him an orphan by the age of 4. He of course
didn't get the Schola treatment since his parents were very far from being
officers in the army.

Thus he had to rely on begging and stealing for a while, before stumbling his
way into the open arms of the church. While Kenneth was not a faithful person at
first he stuck aroud simply because having food and a place to sleep was better
than anything he had for a long long time. And in time the teachings of the god
emperor grew on him and he became a fervent student of the creed.

Finally when the time came that the god emperor required the people of Serenus
to step up and fight for him Kenneth knew it was his place to be there.
