# Kenneth Medland - Creation

## Looks

- Weight: 70kg
- Demeanor: Strict
- Ambition: Finding rank and status inside the ministorum so he can open up his
    own orphanage, as big as possible, possibly even as big as an orphanage
    planet

## Attributes

- BS: 24
- WS: 26
- S: 24
- A: 24
- T: 30
- Int: 35
- Will: 33
- Fel: 31 +5(Class) +3(Regiment) +3(Regimnt)
- Per: 22 -3(Regiment)
- Wounds: 9 + 3 +2 = 15
- Fate: 1 (1-7: 1, 8-9: 2, 10: 3)

## Aptitudes

- Willpower(Regiment)
- Fellowship
- Leadership
- Perception
- Social
- Strength
- Weapon Skill
- Intelligence(double Willpower from Priest & Regiment)

### Rector Erudite

- Willpower(Regiment)
- Fellowship
- Intelligence
- Knowledge
- Toughness(double Willpower from Priest & Regiment)
- Social
- WS
- Perception

## Skills

- Athletics
- Low Gothic
- Operate (Surface) trained (+10)
- Schol. Lore (Beasts)
- Common Lore (War, Imperial Guard)  
- Charm
- Common Lore(Ecclesiarchy)
- Forbidden Lore (Heresy)
- Scholastic Lore (Imperial Creed)

## Starting Talents

- Rapid Reload(Regiment)
- Hatred (Traitors)
- Unshakable Faith
- Weapon Training (Chain, Las, Flame, Low-Tech)

## Equipment

### Rector Erudite

- Good quality hand flamer
- Book of Scripture
- Book of Lore
- Rosarius

- Flak Armor(+4 all)
- Good M36 Lasgun, 4 Chargepacks(Line infantry)

