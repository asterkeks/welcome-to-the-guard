# Weapons

Good Quality Hand-Flamer

Age: 39, recent
Origin: 40, Civilized
Common Pattern - +10 to get related ammo or upgrades
Locked Pattern - +10 to tests to repair it -20 to make modifications
Common/Uncommon: 3 common
Owner: 9, Soldier
+5 WP to resist pinning
1 random customization

Quirk Category: 9, Uncommon Quirk
Quirk: 4, Proud

This machine spirit is proud of its appearance. Any blemish or alteration of
its appearance will anger it deeply, as will attempts to hide the weapon

Normally: +3 BS/+3WS

If angered: -5BS/-5WS, until appeased
