Looks:
A tall man in red robes, not yet very bulky with augmetics.
He prefers to keep his cawl up but what is visible underneath is a thin face, drawn by hunger and privation in his early life.
The only very visible augmetic is the manipulator claw he uses to heft vehicles while maintaining them.
While his face always seem to scowl a hint of a smile can be seen in his eyes when he is working on his machines.

Background:
Valens Psi was born under the name Leonat Coldsmith to a family of manofactorum workers. His career in the admech started when the lack of necessary maintenance in his families hab-unit was discovered to be due to his regular maintenance. Having been given the choice between becoming a servitor for tinkering with admech techonology or taking the theological seminar to join the admech his parents reluctantly allowed him to break the running family tradition by not becoming the 21st inheritor of his fathers position in the manofactorum, giving that honor to his 2nd brother instead.
Always having been more of a tinkerer he had a hard time with the theological parts at first. In time he understood the tennets of the machine god and came for rever them but by then his superiors already had him marked as non-spiritual and unworthy of further advancement in the admech. He saw a chance to escape this dead-lock of his career when the Serenus First was founded, applying for a position as Enginseer with a fresh set of superiors.
He prefers to tend to the machines rather than fight the enemy but seeing their unholy xenos tech can throw him into a battle rage when necessary.

Tech-Priest Enginseer
Aptitudes: BS, int, Knowledge, Strength, Tech, Toughness, WS, Willpower, (WP doubles with regiment) -> Perception
Skills: Common Lore(Admech, Tech), Forbidden Lore(Admech), Logic, Tech-Use +20(+0 spec, +10,+20 ep), Athletics, Low Gothic, Operate (Surface) trained (+10), Schol. Lore (Beasts), Common Lore (War, Imperial Guard), Forbdidden Lore(Archeotech) (ep), Trade(Armourer) (ep)
Talents: Technical Knock, Mechadendrite Use(Weapon, Utility), Weapon Training(Las, Power), Rapid Reload, Total Recall, Master Enginseer
Traits: Mechanicus Implants
One with the land: (+10 Knowledge/Trade/Survival to interactions/identify potentially domesticated beasts & to harvest, cultivate, identify crops) 
Naive: ( -10 on opposed Scrutiny tests vs deceive, first time to gain insanity, gain double the number) 

Equipment:
Sacred unguents, dataslate, combi-tool, Manipulator Mechadendrite
M36 Lasgun + 4 charge packs, 
2 Fragmentation Grenades & 2 Krak Grenades, 
1 Knife, 
Flak Armor: 4 Armor Points on each location, 
1 Uniform + poor-weather gear 
Basic tool set  
Mess kit + water canteen  
Blanket + sleeping bag  
Rechargeable lamp pack  
Grooming kit  
ID tags  
Training handbook  
2 weeks’ ration packs  
Rucksack
Micro-bead (8) 
Preysense Goggles (15) 
Laspistol sidearm + 2 charge packs (5) 
Chrono (2)

Stats:
WS: 28
BS: 35
S: 31 +3(Reg) +3(Reg, Choose) = 37
T: 35 +3(Reg, Choose) 1= 38
AG: 34
Int: 35 -3(Reg) +3(Reg) +5(Spec) +5(Simple) +5(Inter) = 50
Per: 34 -3(Reg) = 31
WP: 34
FEL: 32

Wounds: 13
Fate: 1

Ep Log:

300 Ep speciality
Trade Armourer Known (100)
Total Recall (200)

1000 Ep first mission
Forbidden Knowledge (Archeotech) (100)
Int Simple(100)
Tech Use +10 (200)
Tech Use +20 (300)
= 700

450 Ep second mission (+300 spillover)
Int Inter (250)
Master Enginseer (400)
= 650

Rest: 100

Creation log:
Stat: 2d10 + 20 ⇒ (10, 5) + 20 = 35 Int
Stat: 2d10 + 20 ⇒ (10, 4) + 20 = 34 Wp
Stat: 2d10 + 20 ⇒ (5, 10) + 20 = 35 T
Stat: 2d10 + 20 ⇒ (5, 9) + 20 = 34 BS
Stat: 2d10 + 20 ⇒ (4, 8) + 20 = 32 Fel
Stat: 2d10 + 20 ⇒ (10, 4) + 20 = 34 Ag
Stat: 2d10 + 20 ⇒ (5, 1) + 20 = 26 REROLLED 
Stat: 2d10 + 20 ⇒ (9, 2) + 20 = 31 S
Stat: 2d10 + 20 ⇒ (6, 2) + 20 = 28 WS

Stat Reroll 26: 2d10 + 20 ⇒ (6, 8) + 20 = 34 Per

Speciality:
Tech-Priest Enginseer
Characteristics-Bonus: +5
Aptitudes: BS, int, Knowledge, Strength, Tech, Toughness, WS, Willpower(doubles with
regiment) -> Per
Skills: Common Lore(Admech), Forbidden Lore(Admech), Logic, Tech-Use
Talents: Technical Knock, Mechadendrite Use(Weapon, Utility), Weapon Training(Las, Power)
Traits: Mechanicus Implants
Equipment:
Sacred unguents, dataslate, combi-tool, one utility or ballistic mechadendrite
of choice

Wounds: 8 + 1d5 + 2 ⇒ 8 + (3) + 2 = 13 
Fate 1-7=1,8-9=2,10=3: 1d10 ⇒ 3 = 1 FP

Regiment: 
Additional Aptitude: Willpower 
Starting Talent: Rapid Reload 
Starting Skills: Athletics, Low Gothic, Operate (Surface) trained (+10), Schol. Lore (Beasts), Common Lore (War, Imperial Guard) 
One with the land: (+10 Knowledge/Trade/Survival to interactions/identify potentially domesticated beasts & to harvest, cultivate, identify crops) 
Naive: ( -10 on opposed Scrutiny tests vs deceive, 
first time to gain insanity, gain double the number) 
Favored Weapons: Heavy Stubber and Basic flamer. +10 to acquisition tests to acquire those weapons or the appropriate ammo. 
