[b]"Confirm destruction of Rokk via intact Chimera, order received HQ. Battlegroup Gamma-13 out."[/b]
The Commissar stays with the Vox for a moment to recall the other Chimera
[b]"Margret, this is Commissar Ansley. Seal of Chimera against environment to shield against radiation and proceed to our position. Forward left branch of the canyon. Advance with caution and report enemy contacts. Do not engage unless forced to. Report all interferences"[/b]

He then turns to address the Squad.
[b]"We do. As soon as we have confirmation that the blasted Rokk was actually blasted. It's not as bad as it sounds thought. HQ informed me that Chimeras are shielded against Radiation. On that note - Enginseer I'd like another radiation reading from inside and outside Angelus. "[/b]

[b]"Leutenant Harker, you voxed us from the slope above the Rokk earlier. Is it still possible to observe it from there after the distraction threw some of the overhang on top of it?"[/b]
