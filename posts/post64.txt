[ooc]Usually Lt Harker as the Commander in Chief would make the report, but the GM talking to himself is probably not the intent here, so I'll assume he is engaged with something else[/ooc]
On the Vox:
[b]"HQ, this is Commissar Ansley, reporting for Lt Harker of Battlegroup Gamma 13.
Position is Quadrant AF94/HU12, inside side passage closest to the primary mission objective - Rokk landing craft and ajoined vehicle Manofactorum.
Full mission report pending. Relaying currently important events first.
Currently completed action is under cover infiltration of said canyon to gain access to two distinct connection points to enemy power net with a chimera each to have aquired Tech Priest Anatoly Dashkov overload enemy reactor core in hope of destroying the Rokk in a reactor explosion.
Overload successful. Destruction of Rokk and Manofactorum unable to confirm. Not possible to get close because Gamma radiation the Tech Priest informs me is caused by reactor breaches in space ships.
Ork Mek teleported to our position before breach, disrupted by the Tech Priests effort. Mek engaged and armaments destroyed but fled towards west, disabling Chimera Angelus and Leman Russ Fist. Ork Mek is currently vulnerable if cought.
Encountered dangerous acid spewing worms in canyon caves, probably driven outwards by the radition now. Advise other imperial forces not to engage. acid will melt though vehicle armor in no time.
Status is down to Leman Russ Fist as well as Chimeras Angelus and Margret. Chimera Margret currently outside Canyon, retreated from the worms exiting the caves.
Fist and Angelus unoperable, Tech Priest is enacting field repairs trying to restore motion.
Heavy casualties on the remaining squads Margret, Angelus and Fist. Vehicle mounted weapons all out of ammunation or close to, even if repaired.
HQ confirm receival of brief mission report. Will continue with full report if requested."[/b]
