[dice=Perception +10 vs 31]1d100[/dice]
[ooc]Yep[/ooc]

[b]"Father, I'll take Petes Vengence. MacVyger get us close to Ork Zilla. Dashkov, Multilaser.  Everyone else: keep seven arm away from us."[/b]
The Commissar takes the Melta from Father Bolus and braces the weapon on his knees to shoot out of the exit ramp of the chimera if 7-arm catches up to them.

[ooc]
Ready Action: Take the Melta from Bolus
Brace: Bracing on the knee with view out of the chimeras exit hatch

I say its high time to show them what happens when you take a tank destroying gun to the face.
Rough idea:
full multi-laser salvo from point blank range for +50 on the aim for 5 hits at 2d10+10 pen 2
Followed up by a melta blast with +50 - 20(unlearned) for 2d10+10 Pen24
+ Lots of fun from the other guns

And then we play the fun game of 'how long can you withstand our firepower'
[/ooc]
