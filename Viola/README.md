# Sister Superior Hospitaler Viola
> Grimdark System Character

Chief Medical Staff

# Statline

B35 P38 S31 T34 I37 C28 W37

# Specialities

- Imperium of Man (Loremaster) +1
- Scrutiny (Observer)
- ??? (Tinkerer)
- Quarrel (Spokesman)

# Aptitudes

- Cunning
- Instincts
- (Group) Social

# Profession

- Survivalist
- Believer(2)
- Primary: Warrior(4)
- Primary: Commander(3)
- Secondary, Supplementary: Medic(3+2+2)
  - First Aid 1
  - Diagnose 2
  - Rehabilitate 1
  - Surgery 2
  - Soothing 1