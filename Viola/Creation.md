# Rolls

- 10, 9, 2, 8 => 5, 5, 1, 4 => 20 + 15 = 35
- 1, 2, 6, 7  => 1, 1, 3, 4 => 20 + 9 = 29
- 1, 5, 6, 8  => 1, 3, 3, 4 => 20 + 11 = 31
- 1, 2, 5, 5  => 1, 1, 3, 3 => 20 + 8 = 28
- 3, 6, 2, 10 => 2, 3, 1, 5 => 20 + 11 = 31
- 8, 3, 5, 10 => 4, 2, 3, 5 => 20 + 14 = 34
- 2, 1, 9, 8  => 1, 1, 5, 4 => 20 + 11 = 31

# Statline

B35 P29 S31 T28 I31 C34 W31

# Homeworld: Hiveworld

- -3 Will => W28
- +3 Precision => P32
- +3 Toughness => T31
- Speciality Imperium of Man (Loremaster) +1(Faith)
- Speciality Scrutiny(Observer)
-  (Tinkerer) (probably)
- Aptitude Cunning

# Childhood: Abandoned

- Attribute: +3 Instincts
- Aptitude: Instincts
- Profession: Survivalist (+2)

# Education: Ecclesiarchy Scholam

- +3 Will
- +3 Toughness
- -3 Cunning
- Group Attribute: Social
- Profession: Believer (2)
- Profession: Medic (2)

# Early Career: Soldier

- +3 Precision
- +3 Instincts
- -3 Cunning
- Primary Profession: Warrior(4)
- Secondary Profession: Medic(3)

# Combat Training: Scholam Progenium Trained

- +3 Precision
- Primary Profession: Commander(3)
- Supplementary Proffesion: Medic(2)

# Faith: God Emperor

- +3 Will
- Speciality Imperium of Man (Loremaster) +1

# Faith: Fervent

- +3 Will
- Speciality Quarrel (Spokesman)