# Inventory

- 1x Uniform 
- 1x poor weather gear 
- 1x knife 
- 1x rucksack 
- 1x basic tools 
- 1x mess kit + water canteen 
- 1x blanket + sleeping bag 
- 1x rechargeable lamp pack 
- 1x grooming kit 
- 1x set of cognome tags 
- 1x guard primer 
- 2weeks rations
- 1x Flak Armor(+4 all) 
- 1x Diagnostor 
- 1x Injector(Tranq) 
- 1x Medikit Very much used up 
- 1x M36 Lasgun(line infantry, main weapon)  
- 5x Chargepacks(4xline infantry, 1x trading) ++  
  -1 currently being charged by Doombringers reactor  
  +1 looted the 2nd time around  
- 2x Frag grenades 
- Set of cards - she got those by trading caffein pills while doing sentry work in the trench
- 1x A small metal box with a red cross on it and a half removed label now only reading 'Ord …' and '...er of the Re...' 
- 5x field suture - First Commissar, 2 Monia, Bleeding Trooper 
- 1x bag of blood plasma Use on the first commissar 
- 5x stimm -1 used on Faye, -1 used on Thudd 
- 1x detox 
- 1x pill box "caffeine pills" 
