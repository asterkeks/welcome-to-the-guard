# Inventory

- 1x Uniform 
- 1x poor weather gear 
- 1x knife 
- 1x rucksack 
- 1x basic tools 
- 1x mess kit + water canteen 
- 1x blanket + sleeping bag 
- 1x rechargeable lamp pack 
- 1x grooming kit 
- 1x set of cognome tags 
- 1x guard primer 
- 2weeks rations
- 1x Flak Armor(+4 all) 
- 1x Diagnostor 
- 1x Injector(Tranq) 
- 1x Medikit Very much used up 
- 1x M36 Lasgun(line infantry, main weapon)  
  + Attached Laser Sight 
  + 1x Extra Ammopack
- 2x Frak Grenades 
- 2x Krak Grenades
- Set of cards - she got those by trading caffein pills while doing sentry work in the trench
- 1x A small metal box with a red cross on it and a half removed label now only reading 'Ord …' and '...er of the Re...' 
- 1x detox 
- 1x pill box "caffeine pills" 
- Camo Cloak 
- 5x Field Sutures
