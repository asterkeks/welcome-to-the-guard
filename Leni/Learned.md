# Learned

# Aptitudes
- Willpower(Regiment)
- Ballistic Skill
- Fieldcraft
- Intelligence, 
- Knowledge
- Perception
- Agility(Double willpower aptitude regiment + class) 

# Skills
- Athletics(Line infantry)
- Medicae+30
- Scholastic Lore(Chymestry), 
- Trade(Chymist)
- Low Gothic
- Operate (Surface) trained (+10)
- Tacticia Imperialis +10
- Schol. Lore (Beasts)
- Common Lore (War) +10
- Common Lore (Imperial Guard) +10
- Navigate(Surface)
- Stealth
- Dodge 
- Logic
- Command

# Talents
- Rapid Reload(Regiment),
- Jaded
- Weapon Training(Las)
- Sprint
- Master Chirurgeon 
- Guardian Angel (+1FP to give to others, corruption when not helping others while possible)
- Tactical Insight (Opposed Tacitca Imperialis vs enemy leader, predict enemy movement, giving -10 to evasion checks of enemies)
- Coordinated Attack (Roll Tactica Imperialis, whole squad gains Called-Attack, Roll Tactica Imperialis-10 again to add pen to the attack)

# Traits
- One with the land: +10 Knowledge/Trade/Survival to interactions/identify potentially domesticated beasts & to harvest, cultivate, identify crops 
- Naive: -10 on opposed scrutiny tests vs deceive, first time gain insanity, gain double).
