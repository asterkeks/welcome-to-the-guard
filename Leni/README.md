# Leni

## Aspect 1: combat medic (Training, Duty)
- First Aid
- Your life is in my hands
- Muddy medicine
- Overprotective

## Aspect 2: Gung ho (Personality, Private)
- No. One. Dies
- What gunfight?
- Bargaining hard
- Can't hold back

## Aspect 3: Nurse Training
- I can rest later
- Soothing Smalltalk
- Unfaced by Blood and Gore
- Can't let go

## Aspect 4: Through fire (Defining Event, Mission1 flame the mines..., Private)
- Everyone else is an idiot
- Impromptu Leader
- Surface Navigation
- Easily recognizable
