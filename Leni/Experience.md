# Experience

## Received Experience
- 600 Starting
- 1000
- 450
- 250
- 1000
= 3300 / 2700

1000
1000
1000
250
500 - 
= 3750
1000
=4650

## Learned Advances
### 600
- Medicae Auxilia(250) 
- Int simple(100) 
- Int intermediate(250)

### 1000
- Medicae +10(150, reduced by 50ep for lots of use and matching rp) 
- Master Chirurgeon(400) 
- Sprint(400) 

Left: 50

### 450
- Awareness trained (2Apt, 100ep) 
- Dodge trained (1Apt, 200ep) 
- Commerce Known(100)

### 250
- Stealth known 
- Navigate(Surface) known 

### 1950 (not yet)
- Medicae +20 (2apt, 300ep)
- Medicae +30 (2apt, 400ep)
- Logic(2apt, 100ep)
- Command (0apt, 150ep) (Reduced by 50% from rewards)
- --- change to Tactician
- Tech Use(2apt, 100ep)
- Guardian Angel (T2, 2apt) 300
- Tactical Insight 300xp
- 100 Coordinated Attack
- Schol Lore(Tactica Imperialis) 200xp
- Int+5 (not changing class at 5000exp Milestone)

#### Change to Tactician
Old Aptitudes: Intelligence, Knowledge, Perception, Willpower BS, Fieldcraft, 
New Aptitudes: Intelligence, Knowledge, Leadership, Willpower, BS, Fellowship
New Skills:
- Tacticia Imperialis
- Lore: War, Imperial Guard
New Talents
- Air of Authority
- Heroic Inspiration

- Tactical Insight 300xp
- Battle Plan 500xp
- 100 Coordinated Attack
- BS Simple(1 Apt, 250ep)

Weapons
- Best Laspistol
- Officer's Sabre (Best Quality Mono Sword)

### 1000 (not yet reflected in the profile)
- Medicae +20 (2apt, 300ep) 
- Tech Use(1apt, 200ep)
- Logic(2apt, 100ep)
- Scholastic Lore(Tactics, Tyranids) (2apt, 100ep)
- Command (0apt, 300ep)
---------- switch to sergeant is probably not happening here. Medic fits better
---------- with her academic way of leading
- MAYBE resistance(heat) for the 300 instead of 600(mission reward mission 3)
- Switch to Sergeant
  + command
  + scholastic lore (tactica imperialis)
  + iron discipline/air of authority
  + weapon training(chain, low-tech)
  + apts Defence, Fellowship,Leadership,Perception,Strength,Toughness,Weapon
    Skill
  - apts Ballistic Skill, Fieldcraft, Intelligence, Knowledge, Perception,
    ~~Willpower~~ stays because it is also from the regiment
## Planned at some point
- +5 Agi(1Apt, 250ep) 
- BS Simple(1 Apt, 250ep) 
- Marksman(1 Apt, Tier2 -> 450ep) 

