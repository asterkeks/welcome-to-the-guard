# Leni
Class: Medic  

## Stats

- BS32
- WS25
- S29
- A28
- T25
- I63
- Will32
- Fel30
- Per26
- Hp15/15
- Fate2/2
- Ins0
- Cor7
- 60/60
- 5spare

## Description

Weight: 50kg  
Looks: A tallish red-headed woman with shoulder length hair.  

Size: 1.70m

You can still see the delicate person she was before but basic training has
turned some of that into a more wiry look.  

### As of checkpoint 1
Her hair is now irregularily one to two centimeters shorter than shoulder length
and burnt black at the tips. Her skin seems to be made from 2 different parts:
Slightly burnt black-ish and creepily white. 

### Personality

- Demeanor: Loose Canon - No one dies on her watch, not unless she at least tried
  saving them. If she sees any chance to survive getting to a fallen person she
  will do so.
- Ambition: Seeing the galaxy before returning home so she can return her
  parents continued talks of "if you knew about how the galaxy realy works" with
  interest
- Hatred: Not letting her do her work for whatever reason
- Comrade: Aubray Bialas, Obsessive Died in a papmache bunker which was used for
  target practice by orks with way too many rocket launchers


## Rewards(open)

- 50%xp cost reduce to the next raise of Agility OR Stealth OR Dodge OR Command - used
- 50%xp cost reduce to buy the Resistance [Heat] talent
- Promotion to Sergeant - used
- Astartes Boltgun Casing

## Rolled Mission experiences

- A new world - A new war: 10, later wave

## Topics

- [Mission 1 Inventory](Inventory/Mission01.md)
- [Mission 2 Inventory](Inventory/Mission02.md)
- [Armored Assault Inventory](Inventory/ArmouredAssault.md)
- [Currently learned stuff](Learned.md)
- [Posts](posts/)
- [Experience](Experience.md)
- [Character Creation](Creation.md)
