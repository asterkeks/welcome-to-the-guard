# Generation

## Rolls

- 20 + 2d10 ⇒ 20 + (1, 2) = 23 
- 20 + 2d10 ⇒ 20 + (2, 5) = 27 
- 20 + 2d10 ⇒ 20 + (10, 2) = 32 
- 20 + 2d10 ⇒ 20 + (3, 3) = 26 
- 20 + 2d10 ⇒ 20 + (10, 10) = 40 
- 20 + 2d10 ⇒ 20 + (2, 7) = 29 
- 20 + 2d10 ⇒ 20 + (5, 3) = 28 
- 20 + 2d10 ⇒ 20 + (3, 2) = 25 
- 20 + 2d10 ⇒ 20 + (10, 2) = 32 
- Reroll lowest: 20 + 2d10 ⇒ 20 + (1, 10) = 31 
- Random Wounds: 1d5 ⇒ 5 
- Fate dice: 1d10 ⇒ 8 

## Attribute generation

- BS: 32 = 32 
- WS: 25 = 25 
- S: 26 +3(Regiment) = 29 
- A: 28 = 28 
- T: 25 = 25 
- Int: 40 +5(Class) -3(Regiment) +3(Regiment choose) +3(Mechanized Infantry) +5(Simple) +5(Intermediate) = 58 
- Will: 32 = 32 
- Fel: 27 +3(Regiment choose) = 30 
- Per: 29 -3(Mechanized Infantry) = 26 
- Wounds: 8 +5 +2 = 15 
- Fate: 2 (1-7: 1, 8-9: 2, 10: 3) 
- Insanity: 0 

## Aptitudes
### From Class
- Ballistic Skill
- Fieldcraft
- Intelligence, 
- Knowledge
- Perception
- Willpower

### From Regiment
- Willpower

### Replacements for double aptitudes
- Agility(Willpower) 
