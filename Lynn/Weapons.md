# Weapons

## Hunting Rifle

### Age

This weapon was crafted centuries ago, possibly on a world long since destroyed
and had dozen or more previous owners

Roll twice on the "Previous Owner" table

Requires a Trade [Armorer] +20 check after each mission or it gains the
unreliable property – until seen to by an Enginseer

### From a civilized world

Most imperial planets fall under this category and most common weapon types are
produced in vast quantities within their manufactoria blocks. Only Plasma and
Power weaponry is beyond the production capability of these places.

Weapons from these worlds are made according to precise patterns licensed from
the Mechanicus and the owners can be punished hard if they fail to stick to the
licensed pattern

No stat change

Common Pattern: +10 to any logistic checks to get the weapon, related ammo or
upgrades for it

Locked Pattern: +10 to any tests to repair it but -20 to make any modifications

### Previous Owner

Rolled twice because of old

#### Hive Ganger

The weapon of a hive ganger: dirty, worn, heavily used but deadly

- +3 DMG, proven(2)
- +3 malfunction range
- Lose reliable property

#### Cleric

- Owned by a pious man or woman before, this weapon still has a few prayer
  seals attached to it, bearing long since bleached out prayers and catechisms.

- +5 WP to resist fear based tests (including supernatural effects)

- +10 to tests interacting with the Ecclesiarchy

- -5 to any checks relying on lying

### Vengeful (5)

The weapon has a deeply vengeful nature and is more than ready to punish anyone
that caused injury or insult to it or its wielder.

- +5WS, +2 dmg against targets who insulted or injured the weapon of wielder for
  the remainder of the scene.

- -10 on any tests using this weapon for the next scene if the wielder had a
  chance to avenge an insult or injury if he had the chance to do so but did not

