# Lynn Edwards - Creation

## Looks

- Weight: 46kg
- Demeanor: Psycho - she has a deranged smile and whispers to her weapons before
  taking shots.
- Ambition: Continue to kill enemies until the day she dies

## Attributes

- BS: 36 +5(Class) +3(Regiment choose ) +5(simple) = 49
- WS: 26 = 26
- S: 28 +3(Regiment) = 31
- A: 35 = 35
- T: 32 +3(Regiment choose) = 35
- Int: 29 -3(Regiment) = 26
- Will: 31 = 31
- Fel: 29 = 29
- Per: 31 = 31
- Wounds: 8 + 1 +2 = 11
- Fate: 1 (1-7: 1, 8-9: 2, 10: 3)

## Aptitudes

- Willpower(Regiment)
- Ag
- BS
- Fel
- Fieldcraft
- Finesse
- WS

## Skills

- Athletics(Line infantry)
- Survival(Surface) Navigate(Surface)
- Common Lore (Imperial Guard, War)

## Starting Talents

- Rapid Reload(Regiment)
- Weapon Training(Las, Solid, Primitive)

## Equipment

- Flak Armor(+4 all)
- Good M36 Lasgun, 4 Chargepacks(Line infantry)
- Longlas, 3 Chargepacks(Specialty Weapon)
- 4 Frak Grenades

## Staritng ep (600)

- Deadeye Shot(200)
- BS simple (100)
- Marksman (300)
