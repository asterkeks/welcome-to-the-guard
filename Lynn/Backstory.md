# Lynn Edward - Backstory

Lynn was born into a farmer family. She is an albino and having this minor
mutation would be bad enough at any other place but on a world littered with
tanned farmers she stuck out like a sore thumb. So the other kids tended to play
"hunt the mutant" 'with' her whenever she tried to socialise in any kind of
form.

So what else is there to do on a farmers world? Tend the animals! But with her
albinism staying outside in the sun for prolonged times was nearly impossible as
well, leaving them alone for most of the day while not feeding them.

That is until the other kids found the "mutants' pets" and kicked them half to
death.

Lynn found them suffering and dying and did the only humane thing - she went for
her fathers hunting rifle and ended their suffering. It ended her heart as well
and since then she has a rather deranged smile plastered on her face.

The story doesn't end here thought. She had a gun and she had targets. And she
knew where all those kids usually played. So she went there and got the usual
greeting of "The mutant is here! The mutant is here! Run for your lives the
mutant is here!". But this time around they DID run for their lives when Lynn
brough out the gun.

No one died, because for one she was a kid and the rifle was made for an adult
and she had never fired a gun over any kind range before. But there were plenty
of bullets to go around so not many came out uninjured.

The kids parents tried to bully her dad into compensation later but frankly he
didn't give a crap and chased them off of his farm. No one died so law
enforcement in the backwater place she grew up with didn't actually care either.

And so she had a new friend: her gun. And she found out you won't hit with a gun
when you need to hit with it unless you practice. And so she trained with it.
And trained with it. And trained with it.

Until the Serenus 1st was founded and she knew she had found her calling.
Leaving behind the farm work unsuited for her skin. The army of course had the
resources to give her something against sunburn from slight sun exposure and
they recognized her skill with a rifle.

To her delight she found out that her old friend, her rifle, was fit for duty
alongside her. And she was permitted to carry it as her main armament once she
was selected as sniper.
