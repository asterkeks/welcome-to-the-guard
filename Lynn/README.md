# Lynn Edward

## Looks

- Eyes: Red(Albino)
- Hair: White(Albino)
- Weight: 46kg
- Demeanor: Psycho - she has a deranged smile and whispers to her weapons before
  taking shots.
- Ambition: Continue to kill enemies until the day she dies

## Attributes

- BS: 36 +5(Class) +3(Regiment choose ) +5(simple) +5(Keep Class) = 54
- WS: 26 = 26
- S: 28 +3(Regiment) = 31
- A: 35 +5 (Simple) = 40
- T: 32 +3(Regiment choose) = 35
- Int: 29 -3(Regiment) = 26
- Will: 31 = 31
- Fel: 29 = 29
- Per: 31 = 31
- Wounds: 8 + 1 +2 = 11
- Fate: 1 (1-7: 1, 8-9: 2, 10: 3)

## Aptitudes

- Willpower(Regiment)
- Ag
- BS
- Fel
- Fieldcraft
- Finesse
- WS

## Skills

- Athletics(Line infantry)
- Survival(Surface)
- Navigate(Surface)
- Common Lore (Imperial Guard, War)
- Dodge
- Stealth
- Trade (Armorer)
- Acrobatics

## Talents

- Rapid Reload(Regiment)
- Weapon Training(Las, Solid, Primitive)
- Deadeye Shot(reduce called shot penalty by 10)
- Marksman(No penalties at long and extreme range)
- Sprint
- Sharpshooter (reduce called shot penalty by another 10 = 0)
- Hip Shooting (Full move action can be combined with single shot fire)
- Might Shot (+BsBonus/2 to ranged damage)
- Target Selection (Shoot into Melee without penalty if shot with aim action)

## Equipment

- Flak Armor(+4 all)
- M36 Lasgun, 4 Chargepacks(Line infantry)
- Laspistol, 2 Chargepacks
- Hunting Rifle + 40 Bullets (20 Reloads)
  - Custom Grip (+5 BS)
  - Custom Barrel (+2 to half aim, +4 to full aim)
- 4 Frak Grenades
- 2 Smoke Grenades (Requisitioned)
- Camo Cloak (Requisitioned)
- 2 Fragmentation Grenades &
- 2 Krak Grenades
- 1 Knife
- 1 Uniform
- 1 poor-weather gear
- Basic tool set
- Mess kit
- water canteen
- Blanket
- sleeping bag
- Rechargeable lamp pack
- Grooming kit
- ID tags
- Training handbook
- 2 weeks’ ration packs
- Rucksack
- Micro-bead
- Preysense Goggles
- Chrono

## Damage Log

- p108 4 Damage + Bleeding(1) = 7
- p109 1 Bleed Damange = 6
- p109 Fate point 3 wounds returned, heals bleed = 9
- p109 Lasbolt 4 Damage = 5

## Ammo Log

- 2 Shots - Clear the way before the charge
- 1 Shot - heavy stubber emplacement
- 1 Shot - wannabe SNiper
- 1 Shot - 'not-dead' troopers getting back up behind the Orgry
- 1 Shot - miss HONNEEEY!
- 1 Shot - Murdered the Daemon of Khorne
- 2 Shots - murder people outisde the lascanon tower
- 1 Shot - cover fire for those going inside the lascannon tower
- 1 Shot - Tech Priest Abomination in the lascannon tower
- 5 Shots - Cinematic Mode
- 1 Shot - Sergeant of the squad rushing us on open ground
- 1 Shot - Random mook of the squad rushing us on open ground

## Links

- [Creation](Creation.md)
- [Weapons](Weapons.md)
- [Experience](Experience.md)
- [Backstory](Backstory.md)
