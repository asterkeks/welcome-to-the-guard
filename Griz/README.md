# Griz Findley DEAD

- Height: 1,82m
- Age: 34
- Weight: 73kg
- Demeanor: Nihilist
- Ambition: It used to be to serve the emperor, now he just fights because it
  makes him feel something
- Eye Color: Brown
- Hair Color: Black

## Attributes

- BS: 38 +3(Regiment choose) +5(simple) +5(2nd) = 51
- WS: 30 = 30
- S: 31 +3(Regiment) = 34
- A: 34 +5(simple) +5 (2nd) = 44
- T: 36 +3(Regiment choose) +5(Class) +5(Keep class at 2500) = 49
- Int: 29 -3(Regiment) = 26
- Will: 32 = 32
- Fel: 26 = 26
- Per: 32 +5(simple) +5 (2nd) = 42
- Wounds: 14/14
- Fate: 1/1

## Skills

- Athletics(Line infantry)
- Awareness
- Dodge +10
- Security
- Scholasti
- Lore(Tactica Imperialis)
- Stealth
- Acrobatic +10
- Trade(Armourer)

## Talents

- Rapid Reload(Regiment)
- Quick Draw
- Takedown
- Weapon Training(Las, Low-Tech)
- Nerves of Steel
- Fearless

## Equipment

- Regimental Lasgun (3packs)
- Storm trooper carapace armour(+6 all)
- M36 Lasgun, 4 Chargepacks(Line infantry)
- Preysense Googles (helmet internal
- Good Quality Hellgun + Backpack (Ammo: 150)
  - Custom Grip: +5BS for Griz, -5BS for everyone else using this weapon
  - Modified STock extra +2 for half aim, extra +4 for full aim
  - Trigger Adjustment: didn't work
- Best Quality Mono knife

## Other Topics

- [Creation](Creation.md)
- [Background](Background.md)
- [Experience](Experience.md)
