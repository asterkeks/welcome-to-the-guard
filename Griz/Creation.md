# Griz Findley - Creation

Weight: 73m
Demeanor: Nihilist
Ambition: It used to be to serve the emperor, now he just fights because it
makes him feel something

## Attributes

- BS: 38 +3(Regiment choose) +5(simple) = 46
- WS: 30 = 30
- S: 31 +3(Regiment) = 34
- A: 34 +5(simple) = 39
- T: 36 +3(Regiment choose) +5(Class) = 44
- Int: 29 -3(Regiment) = 26
- Will: 32 =
- Fel: 26 = 26
- Per: 32 +5(simple) = 37
- Wounds: 12 +2 +2 = 14
- Fate: 1 (1-7: 1, 8-9: 2, 10: 3)

### Aptitudes

- Willpower(Regiment)
- Ag
- BS
- Fieldcraft
- Finesse
- Offence
- Toughness

### Skills

- Athletics(Line infantry)
- Awareness
- Dodge
- Security
- Scholasti
- Lore(Tactica Imperialis)
- Stealth

### Starting Talents

- Rapid Reload(Regiment)
- Quick Draw
- Takedown
- Weapon
- Training(Las, Low-Tech)

### Equipment

- Regimental Lasgun (3packs)
- Storm trooper carapace armour(+6 all)
- M36 Lasgun, 4 Chargepacks(Line infantry)
- Preysense Googles (helmet internal
- Good Quality Hellgun + Backpack (Ammo: 150)
- Best Quality Mono knife

### Experience

#### 300 Ep (Start)

BS simple(100)
Per simple(100)
Ag simple(100)

#### 2,750 Ep
