# Griz Findley - Background

## Pre-Enlistment Life

Griz started his life as a regular hiver, living with his grand parents because
both his parents were off to fight the emperors foes somewhere. Apparantly they
did so very effectivly because when they both died he got selected for being
brought up in the schola progenium. He's still not sure which of them was the
officer of what they did to make that happen...

He was rather popular in the schola, being in the scrumball team and getting to
have a fling or two with the sisters in early training. But all things have to
come to an end eventually.

## Reason for Enlistment

He got old enough to put his training into action and got kicked out...
graduated from the schola as Storm Trooper

## Completed Specialist Training

Strom Troopering

## Additional Remarks

Griz lost all his feelings due to a sensory overload when an orc assault on a
hive he was part of the defence forces caused the hive above them to collapse.
He survived inside an air bubble an got excavated after about a week. He doesn't
know it but he was awake for most of that, going crazy with despair until it
overloaded and 'killed' all his feelings.
