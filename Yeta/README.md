# Commissar Yeta

Class: Commissar  

Commissar BS28 WS44 S41 A31 T43 I26 Will44 Fel27 Per32 Hp-2/15 Fate0/0 Ins9 Cor0

## Aptitudes

- Offense
- Toughness
- Social
- Perception
- Ballistic Skill
- Willpower
- Weapon Skill
- Strength

## Skills

- intimidate+20
- Scrutiny+10
- Tech use
- Scholastic Lore (Tactica Imperialis)
- Athletics
- Low Gothic
- Operate (Surface) +10
- Scholastic Lore(beasts)
- Common Lore (war)
- Common Lore (guard) +10

## Talents

- Rapid Reload
- Air of Authority
- Cold hearted
- Weapon Training (Bolt)
- Weapon Training (Chain)
- Weapon Training (Las)
- Double Team ('I' am the team)  
  counts as double team for others with the talent
  instead of the usual benefits it requires a successful hard(+0) intimidate or
  fear roll  On successful roll: The user gains +20 instead of +10 for both
  having the double team talent. The partner however gainst +0 for both having
  the double team talent(normal ganging up bonus and the base +10 from hlving
  the talent are unaffected by this.)

## Traits

Note: Augmetics give +2TB when a hit is scored against a location with an
augmetic

- Augmetics Left Leg
- Augmetics Right Leg
- Augmetics Right Arm
  - built in Power Fist
  - +2AP
- Augmetics Left Lung
- Augmetics Right Lung
- Augmetics Heart
  - Both Lungs + Heart Augmetic combines to having 'good' ches implants. Effect:
      Boon "Reduce crit damage to chest area by 1(min 1)" granted
- Augmetics Right Eye
  - built in binocular
  - built in telescopic sight
  - Red Glare: +10 on Intimidate checks, -10 Charm checks

## Default Equipment

- Good chainsword
- Good bolt pistol
- 1x Uniform
- 1x poor weather gear
- 1x knife
- 1x rucksack
- 1x basic tools
- 1x mess kit + water canteen
- 1x blanket + sleeping bag
- 1x rechargeable lamp pack
- 1x grooming kit
- 1x set of cognome tags
- 1x guard primer
- 2weeks rations
- 1x Flak Armor(+4 all)
- 1x M36 Lasgun(line infantry, main weapon)  
- 4x Chargepacks(4xline infantry) ++  
- 2x Frag grenades

## Description

Weight:
Looks:

Size:

## Topics

[Character Creation](Creation.md)  

## HP Log

- -8 from Khornate chainsword - page 102
- **DEATH by Khornate daemon** canceled by 'watch out sir' from a heroic trooper
    page 103
- -9 from random bullet fired into the smoke - page 104
- +6 from Asbjörns First aid - Page 104
- -3 Heretical Lasbolt - Page 104
- -2 Ammunation from fisted heretical Commissar cooking off

## Insanity Log

- +3, doubled through one with the land. Page 100
- +3, mission checkpoint after khornates and nurgling infested chimera. Page 104
