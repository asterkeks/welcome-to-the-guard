# Creation

## Rolls
### Attributes
Attribute Roll: 2d10 + 20 ⇒ (10, 9) + 20 = 39 Will 
Attribute Roll: 2d10 + 20 ⇒ (4, 2) + 20 = 26 Rerolled 
Attribute Roll: 2d10 + 20 ⇒ (8, 1) + 20 = 29 Int 
Attribute Roll: 2d10 + 20 ⇒ (10, 5) + 20 = 35 Str 
Attribute Roll: 2d10 + 20 ⇒ (4, 3) + 20 = 27 Fel 
Attribute Roll: 2d10 + 20 ⇒ (4, 4) + 20 = 28 BS 
Attribute Roll: 2d10 + 20 ⇒ (10, 5) + 20 = 35 Tougness 
Attribute Roll: 2d10 + 20 ⇒ (4, 10) + 20 = 34 WS 
Attribute Roll: 2d10 + 20 ⇒ (7, 5) + 20 = 32 Perception

#### Reroll 
Attribute Roll: 2d10 + 20 ⇒ (2, 9) + 20 = 31 Ag

### Fate 
Attribute Roll: 1d10 ⇒ 7 -> 1

### Wounds: 
1d5 + 10 ⇒ (3) + 10 = 13

### Stats before regimental mods
28 BS 
34 WS 
35 Str 
31 Ag
35 Toughness 
29 Int 
39 Will 
27 Fel 
32 Perception

### Regimental mods
+3 str 
-3 int 
+3 str (choosable)
+3 T
+2 Wounds

Talent:
- Rapid Reload

Aptitude:
- Willpower

Skills:
- Athletics
- Low Gothic
- Operate (Surface) +10
- Scholastic Lore(beasts)
- Common Lore (war)
- Common Lore (guard)

Traits:
- One with the land (+10 on lore rolls about farming)
- Naive (-10 on opposed scrutiny tests, first insanity gain doubles)

Favored Weapons:
- Heavy Stubber
- Basic Flamer

### Mechanized Infantry
+3 int
-3 Per

### Class
+5 Wp
Aptitudes:
- Offense
- Toughness
- Social
- Perception
- Ballistic Skill
- Willpower
- Weapon Skill

Skills:
- command
- Common Lore(Guard)
- Scholastic Lore (Tactica Imperialis)

Talents:
- Air of Authority
- Unshakeable Faith
- Weapon Training (Bolt)
- Weapon Training (Chain)
- Weapon Training (Las)

Equipment:
- Good chainsword
- Good bolt pistol

### Resolve double gains
- Willpower aptitude from regiment and commissar: Strength
- Common Lore(Guard) from regiment and commissar: Common Lore(Guard) +10

## Special Notes
Fate reduced from 1 to 0 because the character has IC background and got
exploded pretty hard. Has a ton of augmetics instead(roll: 6 augmetics,
including power fist in right arm augmetics)
